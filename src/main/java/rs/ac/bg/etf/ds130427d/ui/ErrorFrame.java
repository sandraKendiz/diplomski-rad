package rs.ac.bg.etf.ds130427d.ui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ErrorFrame extends GlobalFrame {

  private JTextField errorMsg;

  public ErrorFrame(String errorMessage) {
    super();

    errorMsg = new JTextField();
    errorMsg.setText(errorMessage);
    errorMsg.setEnabled(false);
    errorMsg.setEditable(false);
    errorMsg.setBounds(17, 61, 427, 26);
    getContentPane().add(errorMsg);
    errorMsg.setColumns(10);

    JLabel lblNewLabel = new JLabel("An error has occured!");
    lblNewLabel.setForeground(Color.RED);
    lblNewLabel.setBounds(152, 18, 149, 37);
    getContentPane().add(lblNewLabel);
//
//    JButton btnNewButton = new JButton("CLOSE");
//    btnNewButton.setBounds(162, 126, 117, 29);
//    getContentPane().add(btnNewButton);
  }
}
