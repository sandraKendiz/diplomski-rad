package rs.ac.bg.etf.ds130427d.ui;

import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JTextField;
import rs.ac.bg.etf.ds130427d.detection.CameraDetection;

public class CameraFrame extends GlobalFrame {

  private JTextField message;
  public static JLabel numPeopleLabel = new JLabel("0");

  public CameraFrame(Thread thread) {
    super();

    message = new JTextField();
    message.setText("Average number of people detected");
    message.setEnabled(false);
    message.setEditable(false);
    message.setBounds(17, 61, 427, 26);
    getContentPane().add(message);
    message.setColumns(10);

    JLabel lblNewLabel = new JLabel("Video processing started!");
    lblNewLabel.setForeground(Color.RED);
    lblNewLabel.setBounds(152, 18, 200, 37);
    getContentPane().add(lblNewLabel);

//    CameraDetection detection = new CameraDetection();
    thread.start();

//    JLabel numPeopleLabel = new JLabel(Integer.toString(DetectFaceFromCam.getNumOfFaces()));
    numPeopleLabel.setForeground(Color.RED);
    numPeopleLabel.setBounds(152, 101, 149, 37);
    getContentPane().add(numPeopleLabel);

  }
}
