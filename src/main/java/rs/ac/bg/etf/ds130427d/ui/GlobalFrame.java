package rs.ac.bg.etf.ds130427d.ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public abstract class GlobalFrame extends JFrame {

  public GlobalFrame() {
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int height = screenSize.height * 2 / 3;
    int width = screenSize.width * 2 / 3;
    this.setSize(new Dimension(width, height));
    getContentPane().setLayout(null);

    this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
  }
}
