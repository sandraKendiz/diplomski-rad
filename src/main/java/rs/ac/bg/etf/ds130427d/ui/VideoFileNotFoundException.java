package rs.ac.bg.etf.ds130427d.ui;

public class VideoFileNotFoundException extends Exception{

  private String message;

  public VideoFileNotFoundException(String message) {
    super(message);
    this.message = message;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
