package rs.ac.bg.etf.ds130427d.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import rs.ac.bg.etf.ds130427d.detection.CameraDetection;

public class MainFrame extends GlobalFrame {

  public MainFrame() {
    super();

    JButton btnNewButton = new JButton("CAMERA");
    btnNewButton.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
//          DetectFaceFromCam.detectFromCam();
        new CameraFrame(new CameraDetection()).setVisible(true);
      }
    });
    btnNewButton.setBounds(112, 39, 212, 69);
    getContentPane().add(btnNewButton);

    JButton btnVideo = new JButton("VIDEO");
    btnVideo.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        new VideoFilePathFrame().setVisible(true);
      }
    });
    btnVideo.setBounds(112, 142, 212, 69);
    getContentPane().add(btnVideo);
  }
}
