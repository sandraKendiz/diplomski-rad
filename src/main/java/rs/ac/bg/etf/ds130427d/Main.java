package rs.ac.bg.etf.ds130427d;

import rs.ac.bg.etf.ds130427d.properties.ApplicationPropertiesEnum;
import rs.ac.bg.etf.ds130427d.ui.MainFrame;

public class Main {

  public static void main(String[] args) {
    ApplicationPropertiesEnum.readConfig("application.properties");
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        MainFrame mainWindow = new MainFrame();
        mainWindow.setVisible(true);
      }
    });
    System.out.printf("");
  }

}
