package rs.ac.bg.etf.ds130427d.detection;

import java.io.File;
import org.opencv.videoio.VideoCapture;
import rs.ac.bg.etf.ds130427d.ui.VideoFileNotFoundException;

public class VideoDetection extends Thread{

  private String path;

  public VideoDetection(String path) throws VideoFileNotFoundException {
    if (path == null || path.isEmpty()) {
      throw new VideoFileNotFoundException("Video file at path " + path + " not found!");
    }
    File videoFile = new File(path);
    if (!videoFile.exists()) {
      throw new VideoFileNotFoundException("Video file at path " + path + " doesn't exist!");
    }
    this.path = path;
  }

  @Override
  public void run() {
    DetectFace detectFace = new DetectFace(this.path);
    detectFace.detect();
  }
}
