package rs.ac.bg.etf.ds130427d.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ApplicationPropertiesEnum {
  public static String CASCADE;
  public static int DETECT_PERIOD;

  public static void readConfig(String configName) {
    ClassLoader loader = Thread.currentThread().getContextClassLoader();
    Properties properties = new Properties();
    try (InputStream resourceStream = loader.getResourceAsStream(configName)) {
      properties.load(resourceStream);
      CASCADE = properties.getProperty("cascade");
      DETECT_PERIOD = Integer.parseInt(properties.getProperty("detect.period"));
      System.out.println("Loaded application.properties");
    } catch (IOException e) {
      System.out.println("Couldn't open application properties.");
      System.exit(0);
    }
  }

}
