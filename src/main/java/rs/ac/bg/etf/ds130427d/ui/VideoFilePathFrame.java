package rs.ac.bg.etf.ds130427d.ui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import rs.ac.bg.etf.ds130427d.detection.VideoDetection;

public class VideoFilePathFrame extends GlobalFrame {

  private JTextField textField;

  public VideoFilePathFrame() {
    super();

    JButton btnVideo = new JButton("ANALYZE");
    btnVideo.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        try {
//          DetectFaceFromCam.detectFromVideo(textField.getText());
          new CameraFrame(new VideoDetection(textField.getText())).setVisible(true);
        } catch (VideoFileNotFoundException exception) {
          new ErrorFrame(exception.getMessage()).setVisible(true);
        }
      }
    });
    btnVideo.setBounds(112, 142, 212, 69);
    getContentPane().add(btnVideo);

    textField = new JTextField();
    textField.setBounds(17, 61, 427, 26);
    getContentPane().add(textField);
    textField.setColumns(10);

    JLabel lblNewLabel = new JLabel("Enter path to video file");
    lblNewLabel.setBounds(152, 18, 149, 37);
    getContentPane().add(lblNewLabel);

  }
}

