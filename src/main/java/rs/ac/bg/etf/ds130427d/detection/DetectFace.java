package rs.ac.bg.etf.ds130427d.detection;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;
import rs.ac.bg.etf.ds130427d.properties.ApplicationPropertiesEnum;
import rs.ac.bg.etf.ds130427d.ui.CameraFrame;

public class DetectFace {

  private int numOfFaces;
  private VideoCapture videoDevice;

  public DetectFace(int index) {
    nu.pattern.OpenCV.loadLocally();
    this.videoDevice = new VideoCapture(index);
  }

  public DetectFace(String path) {
    nu.pattern.OpenCV.loadLocally();
    this.videoDevice = new VideoCapture(path);
  }

  public void detect() {
    CascadeClassifier cascadeFaceClassifier = new CascadeClassifier(
        ApplicationPropertiesEnum.CASCADE);

    try {
      if (videoDevice.isOpened()) {
        Long startTimeInMillis = System.currentTimeMillis();
        int numPersons = 0;
        int count = 0;
        while (true) {
          Mat frameCapture = new Mat();
          videoDevice.read(frameCapture);

          MatOfRect faces = new MatOfRect();
          cascadeFaceClassifier
              .detectMultiScale(frameCapture, faces, 1.1, 2, 0, new Size(30, 30), new Size());
          numPersons += faces.toArray().length;
          count++;
          if (System.currentTimeMillis() - startTimeInMillis >= 1000 * ApplicationPropertiesEnum.DETECT_PERIOD) {
            startTimeInMillis = System.currentTimeMillis();
            numOfFaces = numPersons / count;
            numPersons = 0;
            count = 0;
            CameraFrame.numPeopleLabel.setText(String.valueOf(numOfFaces));
            System.out.println("Average number of faces per minute = " + numOfFaces);

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH) + 1;
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            File parent = new File(String.valueOf(year), String.valueOf(month));
            if (!parent.exists())
            {
              parent.mkdirs();
            }
            File dayFile = new File(parent, String.valueOf(day));
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(dayFile, true)))
            {
              bufferedWriter.write(calendar.getTime() + "\t\t" + " Average number of people detected " + numOfFaces + "\n");
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
      } else {
        System.out.println("Video can't be opened.");
      }
    } finally {
      if (videoDevice != null) {
        videoDevice.release();
      }
    }
  }
}
